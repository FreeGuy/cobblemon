
plugins {
    base
    id("cobblemon.root-conventions")
}

group = "com.cobblemon.mod"
version = "${project.property("mod_version")}+${project.property("mc_version")}"